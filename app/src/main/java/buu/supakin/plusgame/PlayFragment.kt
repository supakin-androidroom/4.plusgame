package buu.supakin.plusgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingUtil
import buu.supakin.plusgame.databinding.FragmentPlayBinding
import kotlin.random.Random

class PlayFragment : Fragment() {

    private lateinit var  binding : FragmentPlayBinding
    private var answerForQuestion = 0
    private var btnArray = arrayListOf<Button>()
    private var correctScore = 0
    private var wrongScore = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_play, container, false)
        binding.apply {
            btnArray = arrayListOf<Button>(
               btnAnswer1,
                btnAnswer2,
                btnAnswer3
            )
        }

        this.play()
        this.setBtnOnclick()
        return binding.root
    }


    private fun setBtnOnclick () {
        for (btn in btnArray) btn.setOnClickListener { this.getResult(btn.text.toString().toInt()) }
    }

    private fun play () {
        val answer = this.setQuestion()
        val answerIndex = Random.nextInt(0,2)
        val answerArray = arrayListOf<Int>()
        answerForQuestion = answer
        answerArray.add(answer)
        this.setScore()

        for ((index, btn) in btnArray.withIndex()) {
            if (answerIndex == index) {
                btn.text = answer.toString()
            } else {
                val anotherAnswer = this.getAnotherAnswer(answer, answerArray)
                answerArray.add(anotherAnswer)
                btn.text = anotherAnswer.toString()
            }
        }
    }

    private fun getAnotherAnswer (realAnswer: Int, answerArray: ArrayList<Int>) : Int {
        while (true) {
            val anotherAnswer = Random.nextInt(0,realAnswer + 5)
            if (!answerArray.contains(anotherAnswer)) return anotherAnswer
            else continue
        }
    }

    private fun getResult (answer: Int) {
        binding.apply {
            if (answer == answerForQuestion) {
                txtResult.text  = "Correct Answer"
                txtResult.setTextColor(resources.getColor(R.color.colorGreen))
                correctScore++
            } else {
                txtResult.text  = "Incorrect Answer"
                txtResult.setTextColor(resources.getColor(R.color.colorRed))
                wrongScore++
            }
            play()
        }

    }

    private fun setQuestion (): Int {
        val numFirst = Random.nextInt(0, 10)
        val numSecond = Random.nextInt(0, 10)
        this.setQuestionDisplay(numFirst, numSecond)
        return numFirst + numSecond
    }

    private fun setQuestionDisplay (numFirst: Int, numSecond: Int) {
        binding.apply {
            txtNumFirst.text = numFirst.toString()
            txtNumSecond.text = numSecond.toString()
        }
    }

    private fun setScore () {
        binding.apply {
            txtCorrectScore.text = correctScore.toString()
            txtWrongScore.text = wrongScore.toString()
        }

    }

    private fun resetScore () {
        correctScore = 0
        wrongScore = 0
        this.setScore()
    }

}